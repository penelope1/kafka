package ir.pnlp.kafka.producer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Random;

@Slf4j
@Service
@RequiredArgsConstructor
public class TopicProducer {

    @Value("${producer.topic}")
    private String topicName;

    private final KafkaTemplate<String, String> kafkaTemplate;

    public void send(String message) {
        log.info("Payload enviado: {}", message);
        kafkaTemplate.send(topicName, message);
    }

    public void sendRandomInteger() {
        Integer i = new Random().nextInt(100);
        log.info("Payload enviado: {}", i);
        kafkaTemplate.send(topicName, i.toString());
    }

}
