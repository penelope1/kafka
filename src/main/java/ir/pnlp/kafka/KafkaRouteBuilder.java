package ir.pnlp.kafka;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class KafkaRouteBuilder extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        from("file:/home/mmohammadi/_projects/kafka/inputFolder?noop=true&delay=60000")
                .routeId("greetings-route")
                .to("file:/home/mmohammadi/_projects/kafka/outputFolder?delay=60000");
    }
}
