package ir.pnlp.kafka.controller;

import ir.pnlp.kafka.producer.TopicProducer;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/kafka")
public class KafkaController {
    private final TopicProducer topicProducer;

    @GetMapping("/publish/{message}")
    public String publishMessage(@PathVariable("message") final String message) {
        topicProducer.send(message);
        return "Published Successfully";
    }

    @GetMapping("/rand")
    public String rand() {
        topicProducer.sendRandomInteger();
        return "Published Successfully";
    }
}
